import { Color, Menu, Vector2 } from "wrapper/Imports"

const MenuBarsTree = Menu.AddEntryDeep(["Visual", "Bars"])
export const BarsState = MenuBarsTree.AddToggle("State", true, "State script: ON or OFF")

/** ============== Adv. Settings ============== */

export const BarsStateSummoned = MenuBarsTree.AddToggle("Summoned", false, "Show on bear, arc warden clone, meepo clone")
export const BarsBlindState = MenuBarsTree.AddToggle("Colorblind Mode")

/** ============== Mana Bars ============== */

const MenuManaBars = MenuBarsTree.AddNode("Mana")
export const ManaBarState = MenuManaBars.AddToggle("State", true)

const MenuManaTextTree = MenuManaBars.AddNode("Text")
export const ManaTextState = MenuManaTextTree.AddToggle("State", false)
export const ManaColorBar = MenuManaBars.AddColorPicker("Bar color", Color.White)
export const ManaBorderColor = MenuManaBars.AddColorPicker("Inside color", Color.Black)

const ManaSizeVectorTree = MenuManaBars.AddNode("Size")
export const ManaSizeVectorX = ManaSizeVectorTree.AddSlider("Size: X", -1, -50, 50)
export const ManaSizeVectorY = ManaSizeVectorTree.AddSlider("Size: Y", 2, -50, 50)
export const ManaPositionVector = MenuManaBars.AddVector2("Position", new Vector2(1, 0), new Vector2(-100, -100), new Vector2(100, 100))

/** ============== Health Bars ============== */

const MenuHealthBars = MenuBarsTree.AddNode("Health")
export const HealthState = MenuHealthBars.AddToggle("State")
export const HealthSizeVector = MenuHealthBars.AddVector2("Position", new Vector2(0, -5), new Vector2(-50, -50), new Vector2(50, 50))
export const HealthSize = MenuHealthBars.AddSlider("Text size", 16, 5, 50)

/** ============== Roshan Health Bars ============== */
const MenuRoshanHealthBars = MenuBarsTree.AddNode("Roshan Health")
export const RoshanHealthState = MenuRoshanHealthBars.AddToggle("State", true)
export const RoshanHSizeVector = MenuRoshanHealthBars.AddVector2("Size", new Vector2(50, 15), new Vector2(15, 10), new Vector2(50, 20))
export const RoshanHPositionVector = MenuRoshanHealthBars.AddVector2("Position", new Vector2(0, 0), new Vector2(-100, -100), new Vector2(100, 100))
export const RoshanHColorBar = MenuRoshanHealthBars.AddColorPicker("Bar color", Color.White)

// all repeated elements
export const NumberModeHP = MenuHealthBars.AddDropdown("Numbers mode", ["Only HP", "HP/MaxHP", "Only %", "HP/%", "HP/MaxHP/%"], 1)
export const NumberModeMP = MenuManaTextTree.AddDropdown("Numbers mode", ["Only MP", "MP/MaxMP", "Only %", "MP/%", "MP/MaxMP/%"], 1)
export const NumberModeHPRoshan = MenuRoshanHealthBars.AddDropdown("Numbers mode", ["Only HP", "HP/MaxHP", "Only %", "HP/%", "HP/MaxHP/%"], 1)

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["Size", "Размер"],
	["Text", "Текст"],
	["Mana", "Мана"],
	["Bars", "Бары"],
	["Health", "Здоровье"],
	["Health", "Здоровье"],
	["Summoned", "Призваные"],
	["Inside color", "Цвет внутри"],
	["Position", "Позиция"],
	["Bar color", "Цвет бара"],
	["Size: X", "Размер: X"],
	["Size: Y", "Размер: Y"],
	["Text size", "Размер текста"],
	["Position: X", "Позиция: X"],
	["Position: Y", "Позиция: Y"],
	["Numbers mode", "Режим чисел"],
	["Damage health", "Показ урона"],
	["Roshan Health", "Здоровье Рошана"],
	["Only HP", "Только ХП"],
	["HP/MaxHP", "ХП/Макс.ХП"],
	["Only %", "Только %"],
	["HP/%", "ХП/%"],
	["HP/MaxHP/%", "ХП/Макс.ХП/%"],
	["Only MP", "Только МП"],
	["MP/MaxMP", "MP/Макс.МП"],
	["МП/%", "МП/%"],
	["МП/MaxMP/%", "МП/Макс.МП/%"],
	["Show on bear, arc warden clone, meepo clone", "Показывать на медведе, арк вардене клоне, мипо клонах"],
]))
