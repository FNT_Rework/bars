import { Color, RendererSDK, Unit, Vector2 } from "wrapper/Imports"
import { BARSData } from "../data"
import { RectangleX } from "../Helper/Rectangle"
import { HealthSize, HealthSizeVector, NumberModeHP } from "../menu"
import { UnitBarsX } from "../X-Core/Imports"

export let HealthModule = (unit: Unit) => {
	const bars = new UnitBarsX(unit)
	const BarPosition = bars.HealthBarPosition
	if (BarPosition.IsZero())
		return
	const healthBarSize = bars.HealthBarSize
	const HealthBarsText = BARSData.ShowType(NumberModeHP.selected_id, unit.HP, unit.MaxHP)
	const Rectangle = new RectangleX(BarPosition, new Vector2(healthBarSize.x, healthBarSize.y)).Add(HealthSizeVector.Vector)
	const vec3 = new Vector2(Rectangle.pos1.x, Rectangle.pos1.y).AddScalarX((Rectangle.pos2.x - RendererSDK.GetTextSize(HealthBarsText, RendererSDK.DefaultFontName, HealthSize.value).x) / 2)
	RendererSDK.Text(HealthBarsText, vec3, Color.White, RendererSDK.DefaultFontName, HealthSize.value)
}
