import { EventsSDK } from "wrapper/Imports"
import { BARSData } from "../data"
import { BarsStateSummoned } from "../menu"
import { ServiceValidate } from "../Service/Validate"

EventsSDK.on("Tick", () => {
	if (!ServiceValidate.IsInGame)
		return
	BARSData.CacheUnits = BARSData.Units.filter(x => !BarsStateSummoned.value
		? ServiceValidate.IsRealHero(x)
		: ServiceValidate.IsHeroSummoned(x),
	)
})
