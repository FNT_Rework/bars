import { ArrayExtensions, Creep, EventsSDK, Hero, Roshan, RoshanSpawner, SpiritBear } from "wrapper/Imports"
import { BARSData } from "../data"

EventsSDK.on("EntityCreated", entity => {

	if (entity instanceof Roshan)
		BARSData.Roshan = entity

	if (entity instanceof RoshanSpawner)
		BARSData.RoshanSpawner = entity

	if (entity instanceof Hero || entity instanceof SpiritBear)
		BARSData.Units.push(entity)

	if (entity instanceof Creep)
		BARSData.Creeps.push(entity)
})

EventsSDK.on("EntityDestroyed", entity => {

	if (entity instanceof Creep)
		ArrayExtensions.arrayRemove(BARSData.Creeps, entity)

	if (entity instanceof Roshan)
		BARSData.Roshan = undefined

	if (entity instanceof RoshanSpawner)
		BARSData.RoshanSpawner = undefined

	if (entity instanceof Hero || entity instanceof SpiritBear)
		ArrayExtensions.arrayRemove(BARSData.Units, entity)
})
